package cmd

import (
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"path/filepath"

	log "github.com/Sirupsen/logrus"

	"github.com/rocksolidlabs/novm/src/novm/utils"

	"github.com/spf13/cobra"
)

var mkkernelCmd = &cobra.Command{
	Use:     "mkkernel [OUTPUT]",
	Aliases: []string{"mk", "mkk"},
	Short:   "-- Make a new kernel from an local kernel.",
	Long:    `Make a new kernel from an local kernel.`,
	Run: func(cmd *cobra.Command, args []string) {

		debugCmd(cmd)
		output := flagString(cmd, "output")
		if output == "" && len(args) > 0 {
			output = args[0]
		}
		release := flagString(cmd, "release")
		modules := flagString(cmd, "modules")
		bzimage := flagString(cmd, "bzimage")
		vmlinux := flagString(cmd, "vmlinux")
		setup := flagString(cmd, "setup")
		sysmap := flagString(cmd, "sysmap")
		nomodules := flagBool(cmd, "nomodules")
		result := Mkkernel(output, release, modules, bzimage, vmlinux, setup, sysmap, nomodules)
		printResult(cmd, result)
	},
}

// Mkkernel makea a new kernel from an local kernel
func Mkkernel(
	output string, release string, modules string, bzimage string,
	vmlinux string, setup string, sysmap string, nomodules bool) string {
	if output == "" {
		output = utils.TempFileName("", ".ker")
	}
	if release == "" {
		release = utils.SystemRelease()
		if debug {
			log.Printf("Creating kernel image using %s release", release)
		}
	}
	if modules == "" {
		modules = "/lib/modules/" + release
	}
	if bzimage == "" {
		bzimage = "/boot/vmlinuz-" + release
	}
	if vmlinux == "" {
		vmlinuxFile, err := ioutil.TempFile("", "ker")
		if err != nil {
			log.Fatal(err)
		}

		command := exec.Command(utils.LibExec("novm-extract-vmlinux"), bzimage)
		command.Stdout = vmlinuxFile
		command.Stderr = os.Stderr

		if debug {
			log.Printf("Runing 'novm-extract-vmlinux' with bzimage=%q to %q\n",
				bzimage, vmlinuxFile.Name())
		}

		if err := command.Run(); err != nil {
			log.Fatalln(err)
		}
		if err := vmlinuxFile.Close(); err != nil {
			log.Fatalln(err)
		}
		vmlinux = vmlinuxFile.Name()

	}
	if setup == "" {
		setupFile, _ := ioutil.TempFile("", "")
		bzimageFile, _ := os.Open(bzimage)
		io.CopyN(setupFile, bzimageFile, 4096)
		bzimageFile.Close()
		setup = setupFile.Name()
		setupFile.Close()
	}

	if sysmap == "" {
		sysmap = "/boot/System.map-" + release
	}

	// Copy all the files into a single directory.
	tempDir, _ := ioutil.TempDir(os.TempDir(), "tmp")
	command := exec.Command(utils.LibExec("novm-mkinitramfs"), release, modules)
	initrd, _ := os.Create(path.Join(tempDir, "initrd"))
	command.Stdout = initrd
	command.Stderr = os.Stderr

	if err := command.Run(); err != nil {
		log.Fatal(err)
	}
	if err := initrd.Close(); err != nil {
		log.Fatal(err)
	}
	utils.CopyFile(vmlinux, path.Join(tempDir, "vmlinux"))
	utils.CopyFile(sysmap, path.Join(tempDir, "sysmap"))
	utils.CopyFile(setup, path.Join(tempDir, "setup"))

	if nomodules {
		os.MkdirAll(path.Join(tempDir, "modules"), os.ModeDir|0755)
	} else {
		utils.CopyTree(
			modules,
			path.Join(tempDir, "modules"),
			func(name string) (bool, error) {
				return !(name == "source" || name == "build"), nil
			},
			false)
	}
	ioutil.WriteFile(path.Join(tempDir, "release"), []byte(release), 0755)
	utils.Tazdir(tempDir, output, nil, nil)

	absOutput, _ := filepath.Abs(path.Clean(output))
	return "file://" + absOutput
}

func init() {
	RootCmd.AddCommand(mkkernelCmd)

	mkkernelCmd.Flags().String("output", "", "The `OUTPUT` file.")
	mkkernelCmd.Flags().String("release", "", "The kernel `RELEASE` (automated).")
	mkkernelCmd.Flags().String("modules", "", "Path to the kernel `MODULES`.")
	mkkernelCmd.Flags().String("vmlinux", "", "Path to the `VMLINUX` file.")
	mkkernelCmd.Flags().String("bzimage", "", "Path to the compressed image `BZIMAGE`.")
	mkkernelCmd.Flags().String("setup", "", "Path to the `SETUP` header.")
	mkkernelCmd.Flags().String("sysmap", "", "Path to the system map `SYSMAP`.")
	mkkernelCmd.Flags().Bool("nomodules", false, "Don't include modules.")

}
